# vim-keys

_A set of useful xcape-style keys utilzing interception-tools_

## What is it?

- This tool compounds on
<a href="https://gitlab.com/interception/linux/plugins/caps2esc">
caps2esc</a> adding the beginning & end of line motion keys (^ and $) when
single pressing the left and right shift keys, respectively.
- Make what's useful comfortably present, just below your pinkies.

## Why?!

Because caps2esc left something lacking from personal xcape setup, and pressing
`Shift + 4/6` is annoying.

## Dependencies

- <a href="https://gitlab.com/interception/linux/tools">
**Interception Tools**</a>
- **IMPORTANT: Unlike caps2esc, this tool requires `Caps Lock` to be remapped
to `Control` in your `xkb` layout, otherwise modifier chording will not work
properly.**

## Building

```
$ git clone https://gitlab.com/hdcase/vim-keys.git
$ cd vim-keys
$ mkdir build
$ cd build
$ cmake ..
$ make
```

## Execution

`vim-tools` is an <a href="https://gitlab.com/interception/linux/tools">
**Interception Tools**</a> plugin. A suggested `udevmon` job configuration is:

```yaml
- JOB: "intercept -g $DEVNODE | vim-tools | uinput -d $DEVNODE"
  DEVICE:
    EVENTS:
      EV_KEY: [KEY_CAPSLOCK, KEY_LEFTSHIFT, KEY_RIGHTSHIFT]

```

For more information about _Interception Tools_, check out the project's
<a href="https://gitlab.com/interception/linux/tools">website</a>.

## Caveats

As always, there are some caveats:

- Again, you **MUST** remap `Caps Lock` to `Control` in your `xkb` settings for
this tool to work properly.
- `intercept -g` will "grab" the detected devices for exclusive access.
- If you tweak your key repeat settings, check whether they get reset.
  Please check <a href="https://github.com/oblitum/caps2esc/issues/1">
  this report</a> about the resolution.

## License

<a href="https://gitlab.com/hdcase/vim-keys/blob/master/LICENSE.md">
  <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/0/0b/
License_icon-mit-2.svg/120px-License_icon-mit-2.svg.png" alt="MIT"></a>

