#include <stdio.h>
#include <stdlib.h>

#include <unistd.h>
#include <linux/input.h>

// clang-format off
const struct input_event
esc_up          = {.type = EV_KEY, .code =        KEY_ESC, .value = 0},
capslock_up     = {.type = EV_KEY, .code =   KEY_CAPSLOCK, .value = 0},
amp_up          = {.type = EV_KEY, .code =          KEY_6, .value = 0},
dollar_up       = {.type = EV_KEY, .code =          KEY_4, .value = 0},
lshift_up       = {.type = EV_KEY, .code =  KEY_LEFTSHIFT, .value = 0},
rshift_up       = {.type = EV_KEY, .code = KEY_RIGHTSHIFT, .value = 0},
esc_down        = {.type = EV_KEY, .code =        KEY_ESC, .value = 1},
capslock_down   = {.type = EV_KEY, .code =   KEY_CAPSLOCK, .value = 1},
amp_down        = {.type = EV_KEY, .code =          KEY_6, .value = 1},
dollar_down     = {.type = EV_KEY, .code =          KEY_4, .value = 1},
lshift_down     = {.type = EV_KEY, .code =  KEY_LEFTSHIFT, .value = 1},
rshift_down     = {.type = EV_KEY, .code = KEY_RIGHTSHIFT, .value = 1},
esc_repeat      = {.type = EV_KEY, .code =        KEY_ESC, .value = 2},
capslock_repeat = {.type = EV_KEY, .code =   KEY_CAPSLOCK, .value = 2},
amp_repeat      = {.type = EV_KEY, .code =          KEY_6, .value = 2},
dollar_repeat   = {.type = EV_KEY, .code =          KEY_4, .value = 2},
lshift_repeat   = {.type = EV_KEY, .code =  KEY_LEFTSHIFT, .value = 2},
rshift_repeat   = {.type = EV_KEY, .code = KEY_RIGHTSHIFT, .value = 2},
syn             = {.type = EV_SYN, .code =     SYN_REPORT, .value = 0};
// clang-format on

int equal(const struct input_event *first, const struct input_event *second) {
    return first->type == second->type && first->code == second->code &&
           first->value == second->value;
}

int read_event(struct input_event *event) {
    return fread(event, sizeof(struct input_event), 1, stdin) == 1;
}

void write_event(const struct input_event *event) {
    if (fwrite(event, sizeof(struct input_event), 1, stdout) != 1)
        exit(EXIT_FAILURE);
}

int main(void) {
    int capslock_is_down = 0, esc_give_up = 0, lshift_is_down = 0,
        rshift_is_down = 0, amp_give_up = 0, dollar_give_up = 0;
    struct input_event input;

    setbuf(stdin, NULL), setbuf(stdout, NULL);

    while (read_event(&input)) {
        if (input.type == EV_MSC && input.code == MSC_SCAN)
            continue;
        if (input.type != EV_KEY) {
            write_event(&input);
            continue;
        }
        if (capslock_is_down) {
            if (equal(&input, &capslock_down) ||
                equal(&input, &capslock_repeat))
                continue;
            if (equal(&input, &capslock_up)) {
                capslock_is_down = 0;
                if (esc_give_up) {
                    esc_give_up = 0;
                    write_event(&capslock_up);
                    continue;
                }
                write_event(&esc_down);
                write_event(&syn);
                usleep(5000);
                write_event(&esc_up);
                continue;
            }
            if (!esc_give_up && input.value) {
                esc_give_up = 1;
                write_event(&capslock_down);
                write_event(&syn);
                usleep(5000);
            }
        } else if (lshift_is_down) {
            if (equal(&input, &lshift_down) ||
                equal(&input, &lshift_repeat))
                continue;
            if (equal(&input, &lshift_up)) {
                lshift_is_down = 0;
                if (amp_give_up) {
                    amp_give_up = 0;
                    write_event(&lshift_up);
                    continue;
                }
                write_event(&lshift_down);
                write_event(&syn);
                usleep(100);
                write_event(&amp_down);
                write_event(&syn);
                usleep(5000);
                write_event(&amp_up);
                write_event(&syn);
                usleep(100);
                write_event(&lshift_up);
                continue;
            }
            if (!amp_give_up && input.value) {
                amp_give_up = 1;
                write_event(&lshift_down);
                write_event(&syn);
                usleep(5000);
            }
        } else if (rshift_is_down) {
            if (equal(&input, &rshift_down) ||
                equal(&input, &rshift_repeat))
                continue;

            if (equal(&input, &rshift_up)) {
                rshift_is_down = 0;
                if (dollar_give_up) {
                    dollar_give_up = 0;
                    write_event(&rshift_up);
                    continue;
                }
                write_event(&rshift_down);
                write_event(&syn);
                usleep(100);
                write_event(&dollar_down);
                write_event(&syn);
                usleep(5000);
                write_event(&dollar_up);
                write_event(&syn);
                usleep(100);
                write_event(&rshift_up);
                continue;
            }
            if (!dollar_give_up && input.value) {
                dollar_give_up = 1;
                write_event(&rshift_down);
                write_event(&syn);
                usleep(5000);
            }
        } else if (equal(&input, &capslock_down) ||
            equal(&input, &lshift_down) ||
            equal(&input, &rshift_down)) {
            if (equal(&input, &capslock_down))
                capslock_is_down = 1;
            if (equal(&input, &lshift_down))
                lshift_is_down = 1;
            if (equal(&input, &rshift_down))
                rshift_is_down = 1;
            continue;
        }

        write_event(&input);
    }
}
